const express = require('express');
const postsRouter = require("posts.controller");
const es6Renderer = require("express-es6-template-engine");
const MainController = require('main-controller');
const config = require('config.json')
const MongoService = require('mongo.service');
const UserController = require('user-controller.js');


const app = express();
app.engine("html", es6Renderer);
app.set("views", __dirname + "/views");
app.use(express.static("public"));
app.use(express.json());

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

(async () => {
    await new MongoService(config.mongo).init();

    new MainController(app);
    new UserController(app);
    

    app.listen(3000, () => {
        console.log("Server is up!");
    })
})();

app.get("/", (req, res) => {
    res.render("index.html", { locals: { title: "Hello!!" } });
});

app.post("/", (req, res) => {
    console.log(req.body)
    res.json({ message: "It`s post", body: req.body })
});

app.use("/posts", postsRouter);


app.get('/stuff', (req, res) => {
    res.status(404).send("Status 404");
    // next();
})

app.get('/stuff/page', (req, res) => {
    res.send("This is stuff page!");
    // next();
})

app.get('/stuff/data', (req, res) => {
    res.json({ data: "This is stuff data" });
    // next();
})

app.post('/stuff/set', (req, res) => {
    res.contentType('application/json');

    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "No body passed!" });
    } else {
        res.json({ message: "OK" });
    }

})